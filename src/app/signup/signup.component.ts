import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth  } from '@angular/fire/auth';
import { AuthService } from '../auth.service';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  
  email: string;
  password:string;
  pbText:string;
  item1 = false;
  item2 = false;
  item3 = false;
  hide = true;
  eyes = false;
  name: string;
  code = '';
  message = '';
  required = '';
  require = true;
  flag = false;
  specialChar = ['!','@','#','$','%','^','&','*',
                  '(',')','_','-','+','=',"'",'"',
                  ',','/','?','.','>','<','{','}',
                  '[',']','|','~','`',':',';']
  valid = false;
  validation = "";

  showEyes()
  {
    this.eyes = false;
  }

  hideEyes()
  {
    this.eyes = true;
  }
 

  signUp()
  {
    this.flag = false;
    this.valid = false;
    this.require = true;

    if (this.name == null || this.password == null || this.email == null) 
    {
      this.required = "this input is required";
      this.require = false;
    }

  if(this.require)
    {
      for (let char of this.specialChar)
      {
        if (this.password.includes(char))
        {
          this.valid = true;
        }
      }
    }
     if (this.valid && this.require)
    {
      console.log("true;");
      this.authService.signUp(this.email,this.password)
        .then(value => { 
              this.authService.updateProfile(value.user,this.name);
              this.authService.addUser(value.user,this.name, this.email); 
        }).then(value =>{
          this.router.navigate(['/welcome']);
        }).catch(err => {
          this.flag = true;
          this.code = err.code;
          this.message = err.message;
          console.log(err);
        })
    }
    else
    {
      this.validation = "Password must contain special characters";
    }
  }

  passwordLen(){
    let len = this.password.length;
   
    if (len === 0) 
    {
      this.pbText= 'Password is blank';
      
    } 
    else if (len > 0 && len <= 4) 
    {
      this.pbText = 'Too weak';
      this.item1 = true;
    } 
    else if (len > 4 && len <= 8) 
    {
      this.pbText = 'Could be stronger';
      this.item2 = true;
    } 
    else 
    {
      this.pbText = 'Strong password';
      this.item3 = true;
    } 
  }


  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

}
